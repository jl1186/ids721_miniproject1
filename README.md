# Mini-project1 by Jamie

This is a modified version from Jekyll theme on Zola. Some modifications were made to better fit the functionality.

## How it looks like:

![Alt text](demo1.png)
![Alt text](demo2.png)

## License

This theme is free and open source software, distributed under the The MIT License. So feel free to use this Jekyll theme anyway you want.

## Credits to the original creators:

This theme was partially designed with the inspiration from these fine folks
- [Nathan Randecker](https://github.com/nrandecker/particle)
- [Willian Justen](https://github.com/willianjusten/will-jekyll-template)
- [Vincent Garreau](https://github.com/VincentGarreau/particles.js/)
